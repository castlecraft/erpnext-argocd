## Get Started

### Prerequisites

- k8s with ArgoCD
- kubectl access
- argocd cli access

### Try locally

Clone the repo locally and switch to cloned directory

```shell
git clone https://gitlab.com/castlecraft/erpnext-argocd.git
cd erpnext-argocd
```

Start local mariadb.

```shell
docker compose -f local/mariadb.compose.yaml up -d
```

Start in cluster NFS server.

```
kubectl create namespace nfs
kubectl create -f local/nfs-server-provisioner/statefulset.yaml
kubectl create -f local/nfs-server-provisioner/rbac.yaml
kubectl create -f local/nfs-server-provisioner/class.yaml
```

Setup k3d cluster and wait for the cluster to be up and running.

```shell
k3d cluster create devcluster \
  --api-port 127.0.0.1:6443 \
  -p 80:80@loadbalancer \
  -p 443:443@loadbalancer \
  --k3s-arg "--disable=traefik@server:0"
```

Setup ArgoCD and wait for the pods to be up an running.

```shell
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
```

Add ArgoCD Application custom resource

```shell
kubectl apply -f apps/redis-apps.yaml
kubectl apply -f apps/erpnext-app.yaml
```
